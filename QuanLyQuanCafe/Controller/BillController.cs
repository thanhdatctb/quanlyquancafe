﻿using QuanLyQuanCafe.Model;
using QuanLyQuanCafe.Session;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Controller
{
    class BillController
    {
        public List<Bill> GetAll()
        {
            List<Bill> bills = new List<Bill>();
            string sql = "select * from Bill";
            var table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                bills.Add(new Bill(row));
            }    

            return bills;
        }
        public int GetUncheckBillIDByTableTD(int id)
        {
            DataTable data = MyConnection.ExecuteQuery("SELECT * FROM dbo.Bill WHERE idTable = " + id + " AND status = 0");

            if (data.Rows.Count > 0)
            {
                Bill bill = new Bill(data.Rows[0]);
                return bill.id;
            }

            return -1;
        }

        public void CheckOut(int id, int discount, float totalPrice)
        {
            string query = "UPDATE dbo.Bill SET dateCheckOut = GETDATE(), status = 1, " + " discount = " + discount + ", totalPrice = " + totalPrice + " WHERE id = " + id;
            
            MyConnection.ExecuteNonQuery(query);
        }
        public bool AddBillWithTableId(int TableId)
        {
            string sql = "INSERT dbo.Bill( DateCheckIn, DateCheckOut, idTable, status, staffId ) VALUES (GETDATE(), NULL , @idTable , 0 , @staffId )";
            MyConnection.ExecuteNonQuery(sql, new object[] { TableId, MySession.staff.id });
           
            return true;
        }
        public int GetMaxIDBill()
        {
            try
            {
                return (int)MyConnection.ExcuteScalar("SELECT MAX(id) FROM dbo.Bill");
            }
            catch
            {
                return 1;
            }
        }
        public List<Menu> GetListMenuByTable(int id)
        {
            List<Menu> listMenu = new List<Menu>();

            string query = "SELECT f.name, bi.count, f.price, f.price*bi.count AS totalPrice FROM dbo.BillInfo AS bi, dbo.Bill AS b, dbo.Food AS f WHERE bi.idBill = b.id AND bi.idFood = f.id AND b.status = 0 AND b.idTable = " + id;

            DataTable data = MyConnection.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                Menu menu = new Menu(item);
                listMenu.Add(menu);
            }

            return listMenu;
        }
        public Bill GetById(int id)
        {
            string sql = "select * from bill where id = @id";
            DataTable table = MyConnection.ExecuteQuery(sql, new object[] { id }); ;
            if(table.Rows.Count==0)
            {
                return null;
            }
            return new Bill(table.Rows[0]);
        }

    }
}
