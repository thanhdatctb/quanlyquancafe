﻿using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Controller
{
    class UserController
    {
        public int? LogIn(string username, string pass, string table)
        {
            String sql = "select * from " + table + " where username = @username and hashpass = @pass";
            DataTable data = MyConnection.ExecuteQuery(sql, new object[] { username, pass });
            if(data.Rows.Count ==0)
            {
                return null;
            }
            else
            {
               return (int) data.Rows[0]["id"];
            }
        }
    }
}
