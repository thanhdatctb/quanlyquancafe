﻿using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace QuanLyQuanCafe.Controller
{
    class StaffTimeController
    {
        public bool Add(StaffTime staffTime)
        {

            string sql = "insert into stafftime (StaffId, dateCheckIn, dateCheckOut) values ( @staffId , @dateCheckIn , @dateCheckout )";
            MyConnection.ExecuteQuery(sql, new object[] { staffTime.StaffId, staffTime.dateCheckIn, staffTime.dateCheckOut });
            return true;
        }
        public List<StaffTime> GetAll()
        {
            string sql = "select * from staffTime";
            List<StaffTime> staffTimes = new List<StaffTime>();
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                staffTimes.Add(new StaffTime(row));
            }
            return staffTimes;
        }
        public List<StaffTime> GetByStaffId(int id)
        {
            string sql = "select * from staffTime where StaffId = @id ";
            List<StaffTime> staffTimes = new List<StaffTime>();
            DataTable table = MyConnection.ExecuteQuery(sql, new object[] { id });
            foreach (DataRow row in table.Rows)
            {
                staffTimes.Add(new StaffTime(row));
            }
            return staffTimes;
        }
    }
}
