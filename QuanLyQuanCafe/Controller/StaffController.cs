﻿using Microsoft.SqlServer.Server;
using QuanLyQuanCafe.Model;
using QuanLyQuanCafe.Session;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Controller
{
    class StaffController: UserController
    {
        public Staff GetById(int id)
        {
            string sql = "select * from staff where id = @id";
            DataTable table = MyConnection.ExecuteQuery(sql, new object[] { id });
            if(table.Rows.Count==0)
            {
                return null;
            }    
            return new Staff(table.Rows[0]);
        }
        public bool LogIn(String username, string hashpass)
        {
            var id = base.LogIn(username, hashpass, "staff");
            if(id == null)
            {
                return false;
            }
            MySession.staff = this.GetById((int)id);
            MySession.checkIn = DateTime.Now;
            return true;
        }
        public List<Staff> GetAll()
        {
            List<Staff> staffs = new List<Staff>();
            string sql = "select * from staff where status =1";
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                Staff staff = new Staff(row);
                staffs.Add(staff);
            }    
            return staffs;
        }
        public bool Add(Staff staff)
        {
            string sql = "insert into staff (name, role, img, username, hashpass) values ( @name , @role , @img , @username , @hashpass )";
            MyConnection.ExecuteNonQuery(sql, new object[] { staff.name, staff.role, staff.img, staff.username, staff.hashpass});
            return true;
        }

        public bool Edit(Staff staff)
        {
            string sql = "update staff set name = @name , role = @role , img = @img , username = @username  where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { staff.name, staff.role, staff.img, staff.username, staff.id });
            return true;
        }
        public bool Delete (int id)
        {
            string sql  = "update staff set status = 0 where id = @Id";
            MyConnection.ExecuteQuery(sql, new object[] { id });
            return true;
        }
        public bool EditBasic(Staff staff)
        {
            string sql = "update staff set name = @name , role = @role where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { staff.name, staff.role, staff.id });
            return true;
        }
        public bool UpdatePassword(int id, string hashpass)
        {
            string sql = "update staff set  hashpass = @hashpass where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { hashpass, id });
            return true;
        }
    }
}
