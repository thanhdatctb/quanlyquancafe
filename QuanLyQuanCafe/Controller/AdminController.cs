﻿using QuanLyQuanCafe.Model;
using QuanLyQuanCafe.Session;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Controller
{
    class AdminController: UserController
    {
        public bool LogIn(string username, string hashpass)
        {
           if(base.LogIn(username, hashpass, "admin") == null)
            {
                return false;
            }
            int id = (int)base.LogIn(username, hashpass, "admin");
            MySession.admin = GetById(id);
            return true;
        }
        public Admin GetById(int id)
        {
            string sql = "select * from admin where id = @id";
            DataTable data = MyConnection.ExecuteQuery(sql, new object[] { id });
            if(data.Rows.Count ==0)
            {
                return null;
            }
            return new Admin(data.Rows[0]);
        }
    }
}
