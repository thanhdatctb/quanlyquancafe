﻿using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace QuanLyQuanCafe.Controller
{
    class FoodController
    {
        //public List<Food> GetAll()
        //{
        //    List<Food> foods = new List<Food>();
        //    string sql = "select * from Food";
        //    var table = MyConnection.ExecuteQuery(sql);
        //    foreach(DataRow row in table.Rows)
        //    {
        //        Food food = new Food(row);
        //        foods.Add(food);
        //    }    
        //    return foods;
        //}
        public List<Food> GetAll()
        {
            List<Food> staffs = new List<Food>();
            string sql = "select * from food";
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach (DataRow row in table.Rows)
            {
                Food food = new Food(row);
                staffs.Add(food);
            }
            return staffs;
        }
        public bool Add(Food food)
        {
            string sql = "INSERT dbo.Food (name, idCategory, price ) VALUES ( @name , @idCategory , @price )";
            MyConnection.ExecuteNonQuery(sql, new object[] { food.name, food.idCategory, food.price });
            return true;
        }
        public bool Edit(Food food)
        {
            string sql = "update Food set name = @name , idCategory = @idCategory , price = @price where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { food.name, food.idCategory, food.price, food.id });
            return true;
        }
        public bool Delete(int id)
        {
            string sql = "delete from food where id = @id";
            MyConnection.ExecuteQuery(sql, new object[] { id });
            return true;
        }

    }
}
