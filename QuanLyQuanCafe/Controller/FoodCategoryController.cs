﻿using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Controller
{
    class FoodCategoryController
    {
        public List<FoodCategory> GetAll()
        {
            List<FoodCategory> foodCategories = new List<FoodCategory>();
            string sql = "select * from FoodCategory";
            var table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                foodCategories.Add(new FoodCategory(row));
            }    
            return foodCategories;
        }
        public FoodCategory GetById(int id)
        {
            string sql = "select * from FoodCategory where id = @id";
            var table =  MyConnection.ExecuteQuery(sql, new object[] { id });
            if(table.Rows.Count ==0)
            {
                return null;
            }    
            FoodCategory foodCategory = new FoodCategory(table.Rows[0]);
            return foodCategory;
        }
    }
}
