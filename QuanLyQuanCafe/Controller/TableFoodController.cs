﻿using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Controller
{
    class TableFoodController
    {
        public TableFood GetById(int id)
        {
            string sql = "select * from tableFood where id = @id";
            var table = MyConnection.ExecuteQuery(sql, new object[] { id });
            if(table.Rows.Count == 0)
            {
                return null;
            }
            return new TableFood(table.Rows[0]);
            
        }
        public bool Edit(TableFood table)
        {
            string sql = "update tableFood set name = @name where id = @id ";
            MyConnection.ExecuteQuery(sql, new object[] { table.name, table.id });
            return true;
        }
    }
}
