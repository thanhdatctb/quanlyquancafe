﻿using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Controller
{
    class BillInfoController
    {
        public bool Add(BillInfo billInfo)
        {
            string sql = "INSERT dbo.BillInfo(idBill, idFood, count) VALUES ( @idBill , @idFood , @count )";
            MyConnection.ExecuteNonQuery(sql, new object[] { billInfo.idBill, billInfo.idFood, billInfo.count });
            return true;
        }
    }
}
