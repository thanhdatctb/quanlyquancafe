﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Model
{
    class TableFood
    {
        public int id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public TableFood(DataRow row)
        {
            this.id = (int)row["id"];
            this.name = (string)row["name"];
            this.status = (string)row["status"];
        }

        public TableFood( string name, string status, int id=0)
        {
            this.id = id;
            this.name = name;
            this.status = status;
        }
        public TableFood() { }
    }
}
