﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace QuanLyQuanCafe.Model
{
    public class Staff
    {
        public int id { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string img { get; set; }
        public string username { get; set; }
        public string hashpass { get; set; }

        public Staff( string name, string role, string img, string username, string hashpass, int id =0)
        {
            this.id = id;
            this.name = name;
            this.role = role;
            this.img = img;
            this.username = username;
            this.hashpass = hashpass;
        }

        public Staff()
        {
        }
        public Staff(DataRow row)
        {
            this.id = (int)row["id"];
            this.name = (string)row["name"];
            this.role = (string)row["role"];
            this.img = (string)row["img"];
            this.username = (string)row["username"];
            this.hashpass = (string)row["hashpass"];
        }
    }

}
