﻿using QuanLyQuanCafe.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Model
{
    class StaffTime
    {
        
        public int id { get; set; }
        public int StaffId { get; set; }
        public string StaffName { get; set; }
        public DateTime dateCheckIn { get; set; }
        public DateTime dateCheckOut { get; set; }
        
        public StaffTime( int staffId, DateTime dateCheckIn, DateTime dateCheckOut, int id = 0)
        {
            this.id = id;
            this.StaffId = staffId;
            this.StaffName = new StaffController().GetById(staffId).name;
            this.dateCheckIn = dateCheckIn;
            this.dateCheckOut = dateCheckOut;
        }
        public StaffTime() { }
        public StaffTime(DataRow row)
        {
            this.id = (int)row["id"];
            this.StaffId = (int)row["StaffId"];
            this.dateCheckIn = (DateTime)row["dateCheckIn"];
            this.dateCheckOut = (DateTime)row["dateCheckOut"];

            this.StaffName = new StaffController().GetById(StaffId).name;
             
            
        }    
    }
}
