﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QuanLyQuanCafe.Model
{
    class Bill
    {
        public int id { get; set; }
        public DateTime DateCheckIn { get; set; }
        public DateTime? DateCheckOut { get; set; }
        public int idTable { get; set; }
        public int status { get; set; }
        public int staffId { get; set; }
        public float discount { get; set; }
        public float totalPrice { get; set; }
        public int numberOfSeat { get; set; }
        public int numberOfPeople { get; set; }

        public Bill(int id, DateTime dateCheckIn, DateTime dateCheckOut, int idTable, int status, int staffId, float discount, float totalPrice, int numberOfSeat, int numberOfPeople)
        {
            this.id = id;
            DateCheckIn = dateCheckIn;
            DateCheckOut = dateCheckOut;
            this.idTable = idTable;
            this.status = status;
            this.staffId = staffId;
            this.discount = discount;
            this.totalPrice = totalPrice;
            this.numberOfSeat = numberOfSeat;
            this.numberOfPeople = numberOfPeople;
        }

        public Bill(int id, DateTime dateCheckIn, int idTable, int staffId, float discount, float totalPrice, int numberOfSeat, int numberOfPeople)
        {
            this.id = id;
            DateCheckIn = dateCheckIn;
            
            this.idTable = idTable;
            
            this.staffId = staffId;
            this.discount = discount;
            this.totalPrice = totalPrice;
            this.numberOfSeat = numberOfSeat;
            this.numberOfPeople = numberOfPeople;
            this.status = 1;
        }
        public Bill()
        {
        }

        public Bill(DateTime dateCheckIn, DateTime dateCheckOut, int idTable, int status, int staffId, float discount, float totalPrice, int numberOfSeat, int numberOfPeople)
        {
            DateCheckIn = dateCheckIn;
            DateCheckOut = dateCheckOut;
            this.idTable = idTable;
            this.status = status;
            this.staffId = staffId;
            this.discount = discount;
            this.numberOfSeat = numberOfSeat;
            this.numberOfPeople = numberOfPeople;
            this.totalPrice = totalPrice;
        }
        public Bill(DataRow row)
        {
            this.id = (int)row["id"];
            this.DateCheckIn = (DateTime)row["DateCheckIn"];
            //
            if(row["DateCheckOut"].ToString() != "")
            {
                this.DateCheckOut = (DateTime?)row["DateCheckOut"];
            }    
            this.idTable = (int)row["idTable"];
            this.status = int.Parse(row["status"].ToString());
            this.staffId = (int)row["staffId"];
            this.discount = float.Parse(row["discount"].ToString());
            //this.numberOfSeat = int.Parse(row["numberOfSeat"].ToString());
            //this.numberOfPeople = int.Parse(row["numberOfPeople"].ToString());
            this.totalPrice = float.Parse(row["totalPrice"].ToString());
        }

    }
}
