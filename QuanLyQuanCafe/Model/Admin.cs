﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Model
{
    class Admin
    {
        int id { get; set; }
        string username { get; set; }
        string hashpass { get; set; }

        public Admin(string username, string hashpass, int id = 0)
        {
            this.id = id;
            this.username = username;
            this.hashpass = hashpass;
        }
        public Admin(DataRow row)
        {
            this.id = (int) row["id"];
            this.username = (string)row["username"];
            this.hashpass = (string)row["hashpass"];
        }

        public Admin()
        {
        }
    }
}
