﻿using QuanLyQuanCafe.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCafe.Model
{
    class Food
    {
        public int id { get; set; }
        public string name { get; set; }
        public int idCategory { get; set; }
        public string CategoryName { get; set; }
        public float price { get; set; }
        public Food(DataRow row)
        {
            this.id = (int)row["id"];
            this.name = (string)row["name"];
            this.idCategory = (int)row["idCategory"];
            this.price = float.Parse(row["price"].ToString());
            this.CategoryName = new FoodCategoryController().GetById(idCategory).name;
        }

        public Food(string name, int idCategory, float price, int id = 0)
        {
            this.id = id;
            this.name = name;
            this.idCategory = idCategory;
            this.price = price;
            this.CategoryName = new FoodCategoryController().GetById(idCategory).name;
        }
        public Food() { }
    }
}
