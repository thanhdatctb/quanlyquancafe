﻿using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for ListFood.xaml
    /// </summary>
    public partial class ListFood : Window
    {
        private FoodController foodController = new FoodController();
        private StaffController staffController = new StaffController();
        private FoodCategoryController foodCategoryController = new FoodCategoryController();
        public ListFood()
        {
            InitializeComponent();
            LoadData();
            this.cbCategory.ItemsSource = foodCategoryController.GetAll();
            this.cbCategory.DisplayMemberPath = "name";
            this.cbCategory.SelectedValuePath = "Id";
        }
        private void LoadData()
        {
            var data = foodController.GetAll();
            this.dtgvFood.ItemsSource = data;
            
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dtgvFood_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = this.txtName.Text;
            int categoryId = (int)this.cbCategory.SelectedValue;
            float price = float.Parse(this.txtPrice.Text);
            try
            {
                if(this.foodController.Add(new Food(name, categoryId, price)))
                {
                    MessageBox.Show("Thêm món ăn thành công");
                    LoadData();
                }    
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            string name = this.txtName.Text;
            int categoryId = (int)this.cbCategory.SelectedValue;
            float price = float.Parse(this.txtPrice.Text);
            int id = int.Parse(this.txtId.Text);
            try
            {
                if (this.foodController.Edit(new Food(name, categoryId, price,id)))
                {
                    MessageBox.Show("Sửa món ăn thành công");
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            string name = this.txtName.Text;
            int categoryId = (int)this.cbCategory.SelectedValue;
            float price = float.Parse(this.txtPrice.Text);
            int id = int.Parse(this.txtId.Text);
            try
            {
                if (this.foodController.Delete(id))
                {
                    MessageBox.Show("Xóa món ăn thành công");
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
