﻿using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for StaffDetail.xaml
    /// </summary>
    public partial class StaffDetail : Window
    {
        Staff staff = new Staff();
        StaffController staffController = new StaffController();
        StaffTimeController staffTimeController = new StaffTimeController();
        public StaffDetail(Staff staff)
        {
            InitializeComponent();
            this.staff = staff;
            string fileName = "ich_will.mp3";
            //string path = Path.Combine(Environment.CurrentDirectory, @"Data\", fileName);
            //string url = @"pack://application:,,,/Resource/image/avatar\" + this.staff.img;
            try
            {

                this.imgAva.Source = new BitmapImage(new Uri(this.staff.img));
                
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.txtName.Content = this.staff.name;
            this.txtStaffId.Content = this.staff.id;
            this.txtRole.Content = this.staff.role;
            this.dtgvTime.ItemsSource = this.staffTimeController.GetByStaffId(this.staff.id);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                this.staffController.Delete(staff.id);
                this.Close();
                new ListStaff().Show();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            new AddOrEditStaff(this.staff.id).Show();
        }
    }
}
