﻿using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Session;
using QuanLyQuanCafe.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLyQuanCafe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AdminController adminController = new AdminController();
        private StaffController staffController = new StaffController();
        public MainWindow()
        {
            InitializeComponent();

        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnLogIn_Click_1(object sender, RoutedEventArgs e)
        {
            string username = this.txtUserName.Text;
            string hashpass = PasswordController.Sha1Hash(txtPasswordBox.Password);
            if(this.adminController.LogIn(username,hashpass))
            {
               
                var home = new AdminHome();
                home.Show();
                
            }   
            else if(this.staffController.LogIn(username,hashpass))
            {

                var home = new Order();
                home.Show();
            }
            else
            {
                MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng");
            }
        }

    }
}
