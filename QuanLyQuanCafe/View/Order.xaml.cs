﻿using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Model;
using QuanLyQuanCafe.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for Order.xaml
    /// </summary>
    public partial class Order : Window
    {
        private StaffTimeController staffTimeController = new StaffTimeController();
        private TableFoodController tableFoodController = new TableFoodController();
        public Order()
        {
            InitializeComponent();
            this.txtStaffName.Text = MySession.staff.name;
            if(this.tableFoodController.GetById(5).status == "Có người")
            {
                //this.table1.Background = Brushes.DarkOrange;
                this.table5.Content += "\n(Có người)";
            }
            if (this.tableFoodController.GetById(4).status == "Có người")
            {
                //this.table1.Background = Brushes.DarkOrange;
                this.table4.Content += "\n(Có người)";
            }
            if (this.tableFoodController.GetById(3).status == "Có người")
            {
                //this.table1.Background = Brushes.DarkOrange;
                this.table3.Content += "\n(Có người)";
            }
            if (this.tableFoodController.GetById(2).status == "Có người")
            {
                //this.table1.Background = Brushes.DarkOrange;
                this.table2.Content += "\n(Có người)";
            }
            if (this.tableFoodController.GetById(1).status == "Có người")
            {
                //this.table1.Background = Brushes.DarkOrange;
                this.table1.Content += "\n(Có người)";
            }
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime checkout = DateTime.Now;
                StaffTime staffTime = new StaffTime(MySession.staff.id, MySession.checkIn, checkout);
                this.staffTimeController.Add(staffTime);
                string message = MySession.staff.name + " Checkin: " + MySession.checkIn + " Check out: " + checkout;
                MessageBox.Show(message);
                
            }catch(Exception ex)
            {

            }
            MySession.staff = new Model.Staff();
            this.Close();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Bàn 1
            new Menu(1).Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //Bàn 2
            new Menu(2).Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //Bàn 3
            new Menu(3).Show();
            this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            //Bàn 5
            new Menu(5).Show();
            this.Close();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            //Bàn 4
            new Menu(4).Show();
            this.Close();
        }
    }
}
