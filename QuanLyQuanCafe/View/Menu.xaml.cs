﻿using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Model;
using QuanLyQuanCafe.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        private int TableID;
        private TableFood table;
        private double totalPrice;
        private double finalPrice;
        private TableFoodController tableFoodController = new TableFoodController();
        private StaffTimeController staffTimeController = new StaffTimeController();
        private FoodController foodController = new FoodController();
        private BillInfoController billInfoController = new BillInfoController();
        private BillController billController = new BillController();
        private List<Model.Menu> menus = new List<Model.Menu>();
        public Menu(int TableID)
        {
            InitializeComponent();
            this.table = this.tableFoodController.GetById(TableID);
            this.txtStaffName.Content = MySession.staff.name;
            this.txtTime.Content = DateTime.Now;
            this.txtTableNumber.Content = TableID.ToString();
            this.dtgvFood.ItemsSource = this.foodController.GetAll();
            LoadOrder();
        }
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            ;

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            new Order().Show();
        }
        private void LoadOrder()
        {
            this.dtgvBinInfo.ItemsSource = billController.GetListMenuByTable(this.table.id);
            this.menus = billController.GetListMenuByTable(this.table.id);
            List<Model.Menu> listBillInfo = billController.GetListMenuByTable(this.table.id);
            float totalPrice = 0;
            foreach (Model.Menu item in listBillInfo)
            {
                //ListViewItem lsvItem = new ListViewItem(item.FoodName.ToString());
                //lsvItem.SubItems.Add(item.Count.ToString());
                //lsvItem.SubItems.Add(item.Price.ToString());
                //lsvItem.SubItems.Add(item.TotalPrice.ToString());
                totalPrice += item.TotalPrice;

                //lsvBill.Items.Add(lsvItem);
            }
            CultureInfo culture = new CultureInfo("vi-VN");// chuyển đổi sang tiền việt

            this.txtTotalPriceBeforDiscount.Content = totalPrice.ToString("c", culture);// culture
            this.totalPrice = totalPrice;
            this.txtTotalPrice.Content = totalPrice.ToString("c", culture);
        }

        private void number(object sender, RoutedEventArgs e)
        {

        }
        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           try
            {
                if(this.txtTotalPriceBeforDiscount != null)
                {
                    //double totalPrice = double.Parse(this.txtTotalPriceBeforDiscount.Content.ToString());
                    //double totalPrice = double.Parse(this.txtTotalPriceBeforDiscount.Content.ToString(), NumberStyles.Currency);
                    double discount = double.Parse(this.txtDiscount.Text.ToString());
                    double finalTotalPrice = this.totalPrice - (totalPrice / 100) * discount;
                    CultureInfo culture = new CultureInfo("vi-VN");// chuyển đổi sang tiền việt
                    this.txtTotalPrice.Content = finalTotalPrice.ToString("c", culture);// culture
                    double receive = double.Parse(this.txtReceive.Text);
                    double payback = receive - finalTotalPrice;
                    this.txtPayBack.Content = payback.ToString("c", culture);
                }    
                
            }
            catch(Exception ex)
           {
               Console.WriteLine(ex.Message);
           }
            finally
            {

            }
        }
        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            
            int idBill;
            idBill = billController.GetUncheckBillIDByTableTD(table.id);
            int discount = int.Parse(txtDiscount.Text);

            double totalPrice = Convert.ToDouble(txtTotalPrice.Content.ToString().Split(',')[0].Replace(".", ""));
            double finalTotalPrice = totalPrice - (totalPrice / 100) * discount;

            if (idBill != -1)
            {
                //if (MessageBox.Show(string.Format("Thanh toán hóa đơn cho {0}\nTổng tiền - (Tổng tiền / 100) x Giảm giá => {1} - ({1} /100) x {2}  \n\nThành tiền = {3}", table.name, totalPrice, discount, finalTotalPrice), "Thanh toán")alogResult.OK)
                {
                    billController.CheckOut(idBill, discount, (float)finalTotalPrice);
                    
                    //ShowBill(table.ID);                    
                   //LoadTable();
                }
                this.tableFoodController.Edit(table);
                this.Close();
                new Bill(idBill, this.menus, discount, totalPrice).Show();
                //new Order().Show();
            }
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int idBill = this.billController.GetUncheckBillIDByTableTD(this.table.id);
            int foodID = int.Parse(this.txtFoodId.Text);
            int count = int.Parse(this.txtCount.Text);
            BillInfo billInfo = new BillInfo(idBill, foodID, count);
            if (idBill == -1)
            {
                
                this.billController.AddBillWithTableId(this.table.id);
                billInfo.idBill = billController.GetMaxIDBill();
                idBill = this.billController.GetUncheckBillIDByTableTD(this.TableID);
                this.billInfoController.Add(billInfo);
            }
            else
            {
                this.billInfoController.Add(billInfo);
            }

            //ShowBill(table.ID);
            this.tableFoodController.Edit(table);
            LoadOrder();
            //LoadTable();
        }
    }
}
