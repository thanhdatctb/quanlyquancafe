﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for AdminHome.xaml
    /// </summary>
    public partial class AdminHome : Window
    {
        public AdminHome()
        {
            InitializeComponent();
        }

        private void btnStaffManage_Click(object sender, RoutedEventArgs e)
        {
            new ListStaff().Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new ListFood().Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            new ListStaffTime().Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            new Revenue().Show();
        }
    }
}
