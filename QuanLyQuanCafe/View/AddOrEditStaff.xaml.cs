﻿using Microsoft.Win32;
using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for AddOrEditStaff.xaml
    /// </summary>
    public partial class AddOrEditStaff : Window
    {
        private int id;
        private Staff staff;
        private StaffController staffController = new StaffController();
        private FileController fileController = new FileController();
        private string avaImg;
        public AddOrEditStaff(int? id)
        {

            InitializeComponent();
            if(id == null)
            {
                this.staff = null;
                this.btnSubmit.Content = "Thêm";
            }   
            else
            {
                this.staff = this.staffController.GetById((int)id);
                this.txtPass.IsEnabled = false;
                this.txtId.Text = staff.id.ToString();
                this.txtName.Text = staff.name;
                this.txtRole.Text = staff.role;
                this.txtUsername.Text = staff.username;
                //this.imgAvatar.Source = new BitmapImage(new Uri(@"..\..\Resource\image\avatar\"+staff.img));
                this.imgAvatar.Source = new BitmapImage(new Uri(staff.img));
            }
           
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if(this.staff == null)
            {
                try
                {
                    string name = this.txtName.Text;
                    string role = this.txtRole.Text;
                    string username = this.txtUsername.Text;
                    string hashpass = PasswordController.Sha1Hash(this.txtPass.Password);
                    string img = "";
                    if(this.imgAvatar.Source != null)
                    {
                        img = this.imgAvatar.Source.ToString().Split('/').Last();
                    } 
                    
                    Staff staff = new Staff(name, role, this.imgAvatar.Source.ToString(), username, hashpass);
                    if(staffController.Add(staff))
                    {
                        MessageBox.Show("Thêm nhân viên thành công");
                        this.Close();
                        new ListStaff().Show();
                    }    
                }catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
                
            }   
            else
            {
                try
                {
                    int id = int.Parse(this.txtId.Text);
                    string name = this.txtName.Text;
                    string role = this.txtRole.Text;
                    string username = this.txtUsername.Text;
                    string hashpass = PasswordController.Sha1Hash(this.txtPass.Password);
                    string img = "";
                    if (this.imgAvatar.Source != null)
                    {
                        img = this.imgAvatar.Source.ToString().Split('/').Last();
                    }

                    Staff staff = new Staff(name, role, this.imgAvatar.Source.ToString(), username, hashpass, id);
                    if (staffController.Edit(staff))
                    {
                        MessageBox.Show("Sửa nhân viên thành công");
                        this.Close();
                        new ListStaff().Show();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }    
        }

        private void btnUploadImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog opnfd = new OpenFileDialog();//tạo ra 1 cửa sổ chọn file
            opnfd.Filter = "Image Files (*.jpg;*.jpeg;.*.gif;)|*.jpg;*.jpeg;.*.gif";//Lọc và hiển thị các file ảnh có đuôi .jpg, .jpeg,...
            opnfd.ShowDialog();//mở cửa sổ chọn file vừa tạo
            this.imgAvatar.Source =  new BitmapImage(new Uri(opnfd.FileName));//gán biến avatarImg bằng tên file người dùng cọn
            this.fileController.MoveFileToImageFolder(opnfd.FileName);//Copy ảnh vào Folder resource
            //this.avaImg = opnfd.FileName.Split('\\').Last();
            this.avaImg = opnfd.FileName;


            ///this.pbAva.Image = new Bitmap(avatarImg);//chuyển ảnh hiển thị sang ảnh người dùng chọn
        }
    }
}
