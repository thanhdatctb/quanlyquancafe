﻿using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for Bill.xaml
    /// </summary>
    public partial class Bill : Window
    {
        private Model.Bill bill;
        private BillController billController = new BillController();
        public Bill(int id, List<Model.Menu> menus, double discount, double totalPrice)
        {
            InitializeComponent();
            this.bill = billController.GetById(id);
            this.txtMaHoaDon.Content = this.bill.id.ToString();
            this.txtTenNhanVien.Content = new StaffController().GetById(this.bill.staffId).name;
            this.txtTenBan.Content = new TableFoodController().GetById(this.bill.idTable).name;
            this.txtCheckIn.Content = this.bill.DateCheckIn;
            this.txtCheckOut.Content = this.bill.DateCheckIn;
            string sql = "select * from billInfo where idBill = "+id;
            this.dtgvProduct.ItemsSource = menus;
            this.txtKhuyenMai.Content = discount.ToString() + "%";
            CultureInfo culture = new CultureInfo("vi-VN");// chuyển đổi sang tiền việt
            this.txtTongTien.Content = totalPrice.ToString("c", culture);// culture
        }
    }
}
