﻿using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for Revenue.xaml
    /// </summary>
    public partial class Revenue : Window
    {
        private BillController billController = new BillController();
        public Revenue()
        {
            InitializeComponent();
            loadBill();
            
        }
        private void loadBill()
        {
            this.dtgvRevenue.ItemsSource = this.billController.GetAll();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(this.cboption.SelectedIndex.ToString());
            string sql = "select * from Bill";
            switch (this.cboption.SelectedIndex.ToString())
            {
                case "0":
                    // code block
                    sql = "select * from Bill";
                    break;
                case "1":
                    // code block
                    sql = @"SELECT t.name AS [Tên bàn], 
                            DateCheckIn AS [Ngày vào], 
                            DateCheckOut AS [Ngày ra],
                            b.totalPrice AS [Tổng tiền] 
                            FROM dbo.Bill AS b, dbo.TableFood AS t";
                    break;
                case "2":
                    // code block
                    sql = @"SELECT YEAR(DateCheckout) as year, MONTH(DateCheckOut) [Month], 
                             DATENAME(MONTH,DateCheckOut) [Month Name], COUNT(1) [Sales Count], sum(totalPrice) as totalPrice
                            FROM dbo.Bill
                            GROUP BY YEAR(DateCheckOut), MONTH(DateCheckOut), 
                             DATENAME(MONTH, DateCheckOut)";
                    break;
            }
            this.dtgvRevenue.ItemsSource =  MyConnection.ExecuteQuery(sql).DefaultView;
            // MessageBox.Show(this.cboption.Items.RemoveAt(this.cboption.Items.IndexOf(this.cboption.SelectedItem)));
        }
    }
}
