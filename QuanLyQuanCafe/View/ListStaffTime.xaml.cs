﻿using QuanLyQuanCafe.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for ListStaffTime.xaml
    /// </summary>
    public partial class ListStaffTime : Window
    {
        StaffTimeController staffTimeController = new StaffTimeController();
        public ListStaffTime()
        {
            InitializeComponent();
            this.dtgvStaffTime.ItemsSource = this.staffTimeController.GetAll();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
