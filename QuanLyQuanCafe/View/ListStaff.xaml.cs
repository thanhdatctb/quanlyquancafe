﻿using QuanLyQuanCafe.Controller;
using QuanLyQuanCafe.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for ListStaff.xaml
    /// </summary>
    public partial class ListStaff : Window
    {
        private StaffController staffController = new StaffController();
        public ListStaff()
        {
            InitializeComponent();
            //int n = this.dtgvStaff.Columns.Count;
            //this.dtgvStaff.Columns[n].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            LoadData();
        }
        private void LoadData()
        {
            this.dtgvStaff.ItemsSource = staffController.GetAll();
        }
        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(this.txtId.Text);
            string name = this.txtName.Text;
            string role = this.txtRole.Text;
            Staff staff = new Staff();
            staff.id = id;
            staff.name = name;
            staff.role = role;
            try
            {
                if(staffController.EditBasic(staff))
                {
                    MessageBox.Show("Sửa nhân viên thành công");
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnResetPass_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(this.txtId.Text);
            this.Close();
            new UpdatePassword(id).Show();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            new AddOrEditStaff(null).Show();
            this.Close();
        }

        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            Staff staff = this.staffController.GetById((int.Parse(this.txtId.Text)));
            new StaffDetail(staff).Show();
        }
    }
}
