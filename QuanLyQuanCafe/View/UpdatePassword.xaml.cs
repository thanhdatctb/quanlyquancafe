﻿using QuanLyQuanCafe.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyQuanCafe.View
{
    /// <summary>
    /// Interaction logic for UpdatePassword.xaml
    /// </summary>
    public partial class UpdatePassword : Window
    {
        private StaffController staffController = new StaffController();
        private int id;
        public UpdatePassword(int id)
        {
            InitializeComponent();
            this.id = id;
            this.txtId.Text = id.ToString();
            this.txtName.Text = staffController.GetById(id).name;
        }

        private void txtSubmit_Click(object sender, RoutedEventArgs e)
        {
            string hashpass = PasswordController.Sha1Hash(this.txtPass.Password);
            string hashconfirm = PasswordController.Sha1Hash(this.txtConfirm.Password);
            if(hashconfirm!=hashpass)
            {
                MessageBox.Show("Mật khẩu không khớp");
                return;
            }    
            try
            {
                if(staffController.UpdatePassword(id,hashpass))
                {
                    MessageBox.Show("Đặt lại mật khẩu thành công");
                    this.Close();
                    new ListStaff().Show();
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
