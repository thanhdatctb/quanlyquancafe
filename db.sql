﻿use master
go
if (exists(select * from sysdatabases where name = 'QuanLyQuanCafeTranKimBao'))
drop database QuanLyQuanCafeTranKimBao
go
create database QuanLyQuanCafeTranKimBao
go 
use QuanLyQuanCafeTranKimBao
go 
create table admin
(
	id int primary key identity,
	username nvarchar(100),
	hashpass nvarchar(100)
)
go
create table staff
(
	id int primary key identity,
	name nvarchar(100),
	role nvarchar(100),
	img nvarchar(100),
	username nvarchar(100),
	hashpass nvarchar(100),
	status int default 1
)
go
CREATE TABLE FoodCategory
(
	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100) NOT NULL  default N'Chưa đặt tên'
)
go
CREATE TABLE Food
(
	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100),
	idCategory INT NOT NULL,
	price FLOAT NOT NULL DEFAULT 0,
	FOREIGN KEY (idCategory) REFERENCES dbo.FoodCategory(id)
)
go 
CREATE TABLE TableFood
(

	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100)NOT NULL DEFAULT N'Chưa có tên',
	status NVARCHAR(100)NOT NULL DEFAULT N'Trống' 
)
go
CREATE TABLE Bill
(
	id INT IDENTITY PRIMARY KEY,
	DateCheckIn DATE NOT NULL DEFAULT GETDATE(),
	DateCheckOut DATE,
	idTable INT NOT NULL,
	status INT NOT NULL --1: thanh toan || 0 chua thanh toan
	FOREIGN KEY (idTable) REFERENCES dbo.TableFood(id),
	staffId int foreign key references Staff(id),
	discount float default 0,
	totalPrice float default 0,
	numberOfSeat int,
	numberOfPeople int
)
go
CREATE TABLE BillInfo
(
	id INT IDENTITY PRIMARY KEY,
	idBill INT NOT NULL,
	idFood INT NOT NULL,
	count INT NOT NULL DEFAULT 0,
	FOREIGN KEY (idBill) REFERENCES dbo.Bill(id),
	FOREIGN KEY (idFood) REFERENCES dbo.Food(id)
)
go
create table StaffTime
(
	id int identity primary key,
	StaffId int foreign key references Staff(id),
	dateCheckIn DateTime,
	dateCheckOut DateTime,
)
go
--THEM catagory
INSERT dbo.FoodCategory (name) VALUES (N'Trà sữa')--name - nvarchar(100)
INSERT dbo.FoodCategory (name) VALUES (N'Cà phê')
INSERT dbo.FoodCategory (name) VALUES (N'Nước ép')
INSERT dbo.FoodCategory (name) VALUES (N'Trà')
INSERT dbo.FoodCategory (name) VALUES (N'Thức ăn')
go
--THÊM món 
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Trà sữa thái', 1, 29000)
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Trà sữa Koi', 1, 49000)--name - nvarchar(100)
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Cà phê xay', 2, 19000)
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Nước ép xoài', 3, 39000)--idcategory - int
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Trà sen', 4, 39000)
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Cà phê rang', 2, 30000)
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Nước ép táo', 3, 38000)
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Trà cúc', 4, 39000)
INSERT dbo.Food (name, idCategory, price ) VALUES (N'Phở', 5, 59000)
--Thêm admin
go
insert into TableFood(name, status) values
(N'Bàn 1',N'Có người'),
(N'Bàn 2',N'Trống'),
(N'Bàn 3',N'Trống'),
(N'Bàn 4',N'Trống'),
(N'Bàn 5',N'Có người')
go
insert into admin (userName,hashpass)values 
( 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
('admin2', 'd033e22ae348aeb5660fc2140aec35850c4da997')
--THêm staff
go
insert into staff(name, role, img, username, hashpass) values
(N'Nguyễn Văn A',N'Phục vụ bàn','atv.jpg','Anv','d033e22ae348aeb5660fc2140aec35850c4da997'),
(N'Trần Văn Cầm',N'Phục vụ bàn','camtv.png','Camtv','d033e22ae348aeb5660fc2140aec35850c4da997')
--THÊM bill
go
INSERT dbo.Bill (DateCheckIn, DateCheckOut, idTable, status,staffId, numberOfSeat, numberOfPeople, totalPrice) VALUES (GETDATE(),NULL,1,0,1,7,5, 1200)--Chưa chek out- date
INSERT dbo.Bill (DateCheckIn, DateCheckOut, idTable, status,staffId, numberOfSeat, numberOfPeople  ) VALUES (GETDATE(),NULL,2,0,2,8,8)--Chưa chek out - date/ status - int
INSERT dbo.Bill (DateCheckIn, DateCheckOut, idTable, status,staffId, numberOfSeat, numberOfPeople ) VALUES (GETDATE(),NULL,2,1,1,3,4)--Đã chek out- idtable: int
INSERT dbo.Bill (DateCheckIn, DateCheckOut, idTable, status,staffId, numberOfSeat, numberOfPeople ) VALUES (GETDATE(),NULL,3,1,1,3,4)
INSERT dbo.Bill (DateCheckIn, DateCheckOut, idTable, status,staffId, numberOfSeat, numberOfPeople ) VALUES (GETDATE(),NULL,3,0,2,4,4)


go
--THÊM bill info
INSERT dbo.BillInfo(idBill, idFood, count) VALUES (1, 1, 2)--idBill
INSERT dbo.BillInfo(idBill, idFood, count) VALUES (1, 3, 4)--idFood---int
INSERT dbo.BillInfo(idBill, idFood, count) VALUES (1, 5, 1)--count
INSERT dbo.BillInfo(idBill, idFood, count) VALUES (2, 6, 2)
INSERT dbo.BillInfo(idBill, idFood, count) VALUES (3, 5, 2)
go

GO
------
CREATE TRIGGER UTG_UpdateTable
ON dbo.TableFood FOR UPDATE
AS BEGIN
	DECLARE @idTable INT
	DECLARE @status NVARCHAR(100)
	
	SELECT @idTable = id, @status = Inserted.status FROM Inserted
	
	DECLARE @idBill INT
	SELECT @idBill = id FROM dbo.Bill WHERE idTable = @idTable AND status = 0
	
	DECLARE @countBillInfo INT
	SELECT @countBillInfo = COUNT(*) from dbo.BillInfo WHERE idBill = @idBill
	
	IF (@countBillInfo > 0 AND @status <> N'Có người')
		UPDATE dbo.TableFood SET status = N'Có người' WHERE id = @idTable
	ELSE IF (@countBillInfo <= 0 AND @status <> N'Trống')
		UPDATE dbo.TableFood SET status = N'Trống' WHERE id = @idTable

END
go
select * from staff
go
SELECT f.name, bi.count, f.price, f.price*bi.count AS totalPrice FROM dbo.BillInfo AS bi, dbo.Bill AS b, dbo.Food AS f WHERE bi.idBill = b.id AND bi.idFood = f.id AND b.status = 0 AND b.idTable = 1

SELECT t.name AS [Tên bàn], 
                            DateCheckIn AS [Ngày vào], 
                            DateCheckOut AS [Ngày ra],
                            b.totalPrice AS [Tổng tiền] 
                            FROM dbo.Bill AS b, dbo.TableFood AS t
SELECT YEAR(DateCheckout) as year, MONTH(DateCheckOut) [Month], 
                             DATENAME(MONTH,DateCheckOut) [Month Name], COUNT(1) [Sales Count], sum(totalPrice) as totalPrice
                            FROM dbo.Bill
                            GROUP BY YEAR(DateCheckOut), MONTH(DateCheckOut), 
                             DATENAME(MONTH, DateCheckOut)